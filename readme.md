
Phops JSON
==========


Introduction
------------

Although JSON encode/decode is part of PHP by default this library provides a more (subjectively)
expected default behavior.


Basic usage
-----------

```bash
composer require phops/json
```


Functions
---------


### \Phops\JSON::encode

```php
$json = \Phops\JSON::encode($value);
```

Encodes a value to JSON. If passed in value cannot be encoded a `\Phops\JSONException` is thrown.


### \Phops\JSON::decode

```php
$value = \Phops\JSON::decode($json);
```

Decodes a value to JSON. If passed in value cannot be decoded a `\Phops\JSONException` is thrown.


License
-------

Phops JSON is licensed under the [MIT license](/license.txt).
