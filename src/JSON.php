<?php

namespace Phops;

class JSON
{
    /**
     * Encode a value to JSON.
     *
     * @param mixed $value Value to encode
     *
     * @return string Encoded JSON
     */
    public static function encode($value)
    {
        // @todo Fix JSON_NUMERIC_CHECK
        $encoded = json_encode(
            $value,
            JSON_UNESCAPED_UNICODE
            // Improve security by also encoding `"`, `<`, `>`, `'` and `&` in case output is embedded into HTML.
            | JSON_HEX_QUOT | JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS
        );
        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new JSONException('Unable to encode json: ' . json_last_error_msg());
        }
        return $encoded;
    }

    /**
     * Decode a JSON string.
     *
     * @param string $value Value to decode
     *
     * @return mixed Decoded value
     */
    public static function decode($value)
    {
        if ('' == trim($value)) {
            throw new JSONException('Unable to decode json: Empty string is not a valid json.');
        }
        $decoded = json_decode($value, false, 512, JSON_BIGINT_AS_STRING);
        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new JSONException('Unable to decode json: ' . json_last_error_msg());
        }
        $toJsonObject = function ($value) use (&$toJsonObject) {
            if (is_object($value)) {
                $subValues = [];
                foreach ($value as $name => $subValue) {
                    $subValues[$name] = $toJsonObject($subValue);
                }
                return new JSONObject($subValues);
            }
            if (is_array($value)) {
                return array_map($toJsonObject, $value);
            }
            return $value;
        };

        return $toJsonObject($decoded);
    }
}
